'use strict';

const { createValidation } = require('../../tools');

const bodySchema = {
  type: 'object',
  properties: {
    token: { type: 'string' }
  },
  required: ['token'],
  additionalProperties: false
};

const validateBody = createValidation(bodySchema);

function createDepositAddressRequest(dependencies) {
  const {
    accountProxy
  } = dependencies;
  return (request, response) => {
    validateBody(request.body);
    request.withBodySignature = true;
    return accountProxy.web(request, response);
  };
}

module.exports = { createDepositAddressRequest };
