'use strict';

const { createValidation } = require('../../tools');

const bodySchema = {
  type: 'object',
  properties: {
    amount: { type: 'string' },
    address: { type: 'string' },
    token: { type: 'string' }
  },
  required: ['amount', 'address', 'token'],
  additionalProperties: false
};

const validateBody = createValidation(bodySchema);

function createWithdrawalCreation(dependencies) {
  const {
    accountProxy
  } = dependencies;
  return (request, response) => {
    validateBody(request.body);
    request.withBodySignature = true;
    return accountProxy.web(request, response);
  };
}

module.exports = { createWithdrawalCreation };
