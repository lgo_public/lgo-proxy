'use strict';

function createAuthenticationProxyMiddleware(dependencies) {
  const { headersFactory, logger } = dependencies;

  return (proxyRequest, request) => {
    logger.debug('Adding authentication to headers');
    const headers = headersFactory(proxyRequest.context.targetUrl, proxyRequest.context.withBodySignature ? request.body : null);
    Object.entries(headers).forEach(([key, value]) =>
      proxyRequest.setHeader(key, value)
    );
  };
}

module.exports = { createAuthenticationProxyMiddleware };
