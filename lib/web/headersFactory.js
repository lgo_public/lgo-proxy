'use strict';

function createHeadersFactory(dependencies) {
  const { configuration, createUtcDate, signer } = dependencies;
  return (targetUrl, body) => {
    const now = createUtcDate();
    const timestamp = now.getTime();
    const accessKey = configuration.accessKey;
    const urlToSign = createUrlToSign(targetUrl);
    const toSign = body ? `${timestamp}\n${urlToSign}\n${JSON.stringify(body)}` : `${timestamp}\n${urlToSign}`;
    const signature = signer.sign(toSign);
    return {
      'X-LGO-DATE': timestamp,
      Authorization: `LGO ${accessKey}:${signature}`
    };
  };

  function createUrlToSign(url) {
    return url
      .replace('http://', '')
      .replace('https://', '')
      .replace('ws://', '')
      .replace('wss://', '');
  }
}

module.exports = { createHeadersFactory };
